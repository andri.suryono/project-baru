<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>HARI 1 : Berlatih HTML</title>
</head>
<body>
    <h1>Buat Account Baru !</h1>
    <h2>Sign Up Form</h2>
    <form action="/post" method="post">
        @csrf
        <label for="namadepan">First Name :</label>
            <br>
            <input type="text" name="namadepan" id="namadepan">
            <br><br>
        <label for="namabelakang">Last Name :</label>
            <br>
            <input type="text" name="namabelakang" id="namabelakang">
            <br><br>
        <label>Gender :</label>
            <br>
            <input type="radio" id="male" name="gender" value="male">
            <label for="male">Male</label><br>
            <input type="radio" id="female" name="gender" value="female">
            <label for="female">Female</label><br>
            <input type="radio" id="other" name="gender" value="other">
            <label for="other">Other</label>
            <br><br>
        <label for="negara">Nationality :</label>
            <br>
            <select name="negara" id="negara">
                <option value="Indonesian">Indonesia</option>
                <option value="English">English</option>
                <option value="China">China</option>
                <option value="Arab">Arab</option>
            </select>
            <br><br>
        <label for="negara">Language Spoken :</label>
            <br>
            <input type="checkbox" id="bhs_indonesia" name="bhs_indonesia" value="Bahasa Indonesia">
            <label for="bhs_indonesia">Bahasa Indonesia</label><br>
            <input type="checkbox" id="bhs_inggris" name="bhs_inggris" value="English">
            <label for="bhs_inggris">English</label><br>
            <input type="checkbox" id="other" name="other" value="Other">
            <label for="other">Other</label><br>
            <br>
        <label for="bio">Bio :</label>
            <br>
            <textarea name="bio" id="bio" cols="30" rows="10"></textarea>
            <br>
        <input type="submit" value="Sign Up" name="daftar">
    </form>

    
</body>
</html>