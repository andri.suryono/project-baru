<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLikeDislikePertanyaanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('like_dislike_pertanyaan', function (Blueprint $table) {
            $table->Increments('id');
            $table->unsignedInteger('pertanyaan_id');
            $table->unsignedInteger('profil_id');
            $table->foreign('profil_id')
            ->references('id')->on('profil')
            ->onDelete('restrict')
            ->onUpdate('cascade');
            $table->foreign('pertanyaan_id')
            ->references('id')->on('pertanyaan')
            ->onDelete('restrict')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('like_dislike_pertanyaan');
    }
}
