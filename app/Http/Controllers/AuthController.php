<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    function register(){
        return view('register');
    }

    function welcome2(){
        return view('welcome2');
    }

    function handlePost(Request $request){
        $namadepan = $request["namadepan"];
        $namabelakang = $request["namabelakang"];
        return view('welcome2', compact('namadepan', 'namabelakang'));
    }
}
