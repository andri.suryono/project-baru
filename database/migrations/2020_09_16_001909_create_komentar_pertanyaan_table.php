<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKomentarPertanyaanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('komentar_pertanyaan', function (Blueprint $table) {
            $table->Increments('id');
            $table->string('isi', 255);
            $table->date('tanggal_dibuat');
            $table->unsignedInteger('pertanyaan_id');
            $table->unsignedInteger('profil_id');
            $table->foreign('profil_id')
            ->references('id')->on('profil')
            ->onDelete('restrict')
            ->onUpdate('cascade');
            $table->foreign('pertanyaan_id')
            ->references('id')->on('pertanyaan')
            ->onDelete('restrict')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('komentar_pertanyaan');
    }
}
