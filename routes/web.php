<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/', 'PostController@home');

// Route::get('/register', 'AuthController@register');
// Route::get('/welcome2', 'AuthController@welcome2');

// Route::post('/post', 'AuthController@handlePost');

// Route::get('/master', function() {
//     return view('adminlte.master');
// });

// Route::get('/items', function(){
//     return view('items.index');
// });

// Route::get('/items/create', function(){
//     return view('items.create');
// });

Route::get('/', function(){
    return view('content.index');
});

Route::get('/data-tables', function(){
    return view('content.data-tables');
});

