<?php
    require('animal.php');
    require('ape.php');
    require('frog.php');
    $sheep = new Animal("shaun");
    
    echo $sheep->name .'<br>'; // "shaun"
    echo $sheep->legs .'<br>'; // 2
    echo $sheep->cold_blooded .'<br>'; // false
    echo "<br>";
    
    $sungokong = new Ape("kera sakti");
    echo $sungokong->name .'<br>';
    echo $sungokong->cold_blooded .'<br>'; // false
    echo $sungokong->yell() .'<br>';; // "Auooo"


    echo "<br>";
    $kodok = new Frog("buduk");
    echo $kodok->name .'<br>'; // ""
    echo $kodok->cold_blooded.'<br>'; 
    echo $kodok->jump() ; //

?>